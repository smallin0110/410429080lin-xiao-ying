﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication1.Controllers
{
    public class BMIController : Controller
    {
        // GET: BMI
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(float height,float weight)
        {
            float m_height = height / 100;
            float bmi = weight / (m_height * m_height);
            string level = "";

            if (bmi<18.5) {
                level = "過瘦";
            }
            else if (18.5<=bmi && bmi<24)
            {
                level = "正常";
            }
            if (bmi >=18)
            {
                level = "過重";
            }

            ViewBag.BMI = bmi;
            ViewBag.level = level;

            return View();
        }
    }
}